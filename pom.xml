<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ u-print
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.0.RELEASE</version>
        <relativePath/>
    </parent>

    <groupId>ch.ge.ve.uprint</groupId>
    <artifactId>u-print-parent</artifactId>
    <version>0.0.1</version>
    <packaging>pom</packaging>

    <name>U-Print Parent</name>
    <description>Off-line tool for printer authorities and printers</description>
    <organization>
        <name>OCSIN-SIDP</name>
    </organization>

    <modules>
        <module>u-print-business</module>
        <module>u-print-gui</module>
    </modules>

    <distributionManagement>
        <snapshotRepository>
            <id>chvote-snapshots</id>
            <name>CHVote Snapshots</name>
            <url>${env.MVN_DIST_SNAPSHOTS_URL}</url>
        </snapshotRepository>
        <repository>
            <id>chvote</id>
            <name>CHVote Releases</name>
            <url>${env.MVN_DIST_RELEASES_URL}</url>
        </repository>
    </distributionManagement>

    <properties>
        <!-- Build parameters -->
        <java.version>11</java.version>
        <sourceEncoding>UTF-8</sourceEncoding>
        <resourceEncoding>ISO-8859-1</resourceEncoding>
        <project.build.sourceEncoding>${sourceEncoding}</project.build.sourceEncoding>
        <project.build.resourceEncoding>${resourceEncoding}</project.build.resourceEncoding>
        <build.timestamp>${maven.build.timestamp}</build.timestamp>
        <maven.build.timestamp.format>dd.MM.yyyy HH:mm</maven.build.timestamp.format>

        <!-- SonarQube parameters -->
        <sonar.jacoco.reportPaths>target/jacoco.exec</sonar.jacoco.reportPaths>
        <sonar.projectName>U-Print</sonar.projectName>

        <!-- Plugins versions -->
        <gmavenplus-plugin.version>1.6.2</gmavenplus-plugin.version>
        <jacoco-maven-plugin.version>0.8.2</jacoco-maven-plugin.version>

        <!-- Plugin JAR dependencies versions -->
        <asm.version>7.0</asm.version>

        <!-- JAR dependencies versions -->

        <!-- CHVote dependencies versions -->
        <chvote-fx-common.version>0.0.8</chvote-fx-common.version>
        <chvote-pact.version>0.0.5</chvote-pact.version>
        <chvote-protocol-core.version>1.0.16</chvote-protocol-core.version>
        <chvote-protocol-model.version>1.0.6</chvote-protocol-model.version>
        <chvote-interfaces.version>2.0.11</chvote-interfaces.version>
        <chvote-model-converter.version>1.0.19</chvote-model-converter.version>
        <file-namer.version>0.1.2</file-namer.version>
        <chvote-crypto.version>1.2.0</chvote-crypto.version>
        <jackson-serializer.version>1.0.1</jackson-serializer.version>

        <!-- JavaFX dependencies version -->
        <javafx.version>11.0.1</javafx.version>
        <jfoenix.version>9.0.2</jfoenix.version>
        <fontawesomefx-materialdesignfont.version>2.0.26-9.1.2</fontawesomefx-materialdesignfont.version>
        <fontawesomefx-materialicons.version>2.2.0-9.1.2</fontawesomefx-materialicons.version>

        <!-- Additional dependencies versions -->
        <bouncycastle.version>1.60</bouncycastle.version>
        <awaitility.version>3.1.2</awaitility.version>
        <guava.version>27.0-jre</guava.version>
        <stax2-api.version>3.1.4</stax2-api.version>
        <woodstox-core.version>4.4.1</woodstox-core.version>
        <!-- TODO See: https://github.com/spring-projects/spring-boot/issues/15110 -->
        <dropwizard-metrics.version>3.2.6</dropwizard-metrics.version>

        <!-- Test dependencies versions -->
        <objenesis.version>3.0.1</objenesis.version>
        <cglib.version>3.2.9</cglib.version>
        <spock.version>1.2-groovy-2.5</spock.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- CHVote dependencies -->
            <dependency>
                <groupId>ch.ge.ve.ui</groupId>
                <artifactId>chvote-fx-common</artifactId>
                <version>${chvote-fx-common.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>pact-back-contract</artifactId>
                <version>${chvote-pact.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-algorithms</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-support</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>chvote-protocol-model</artifactId>
                <version>${chvote-protocol-model.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-interfaces-jaxb</artifactId>
                <version>${chvote-interfaces.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-model-converter-api</artifactId>
                <version>${chvote-model-converter.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>file-namer</artifactId>
                <version>${file-namer.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-model-converter-impl</artifactId>
                <version>${chvote-model-converter.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>crypto-api</artifactId>
                <version>${chvote-crypto.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>crypto-impl</artifactId>
                <version>${chvote-crypto.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>jackson-serializer</artifactId>
                <version>${jackson-serializer.version}</version>
            </dependency>

            <!-- JavaFX dependencies -->
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-controls</artifactId>
                <version>${javafx.version}</version>
            </dependency>
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-base</artifactId>
                <version>${javafx.version}</version>
            </dependency>
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-fxml</artifactId>
                <version>${javafx.version}</version>
            </dependency>
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-graphics</artifactId>
                <version>${javafx.version}</version>
                <classifier>win</classifier>
            </dependency>
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-graphics</artifactId>
                <version>${javafx.version}</version>
                <classifier>mac</classifier>
            </dependency>
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-graphics</artifactId>
                <version>${javafx.version}</version>
                <classifier>linux</classifier>
            </dependency>
            <dependency>
                <groupId>com.jfoenix</groupId>
                <artifactId>jfoenix</artifactId>
                <version>${jfoenix.version}</version>
            </dependency>
            <dependency>
                <groupId>de.jensd</groupId>
                <artifactId>fontawesomefx-materialdesignfont</artifactId>
                <version>${fontawesomefx-materialdesignfont.version}</version>
            </dependency>
            <dependency>
                <groupId>de.jensd</groupId>
                <artifactId>fontawesomefx-materialicons</artifactId>
                <version>${fontawesomefx-materialicons.version}</version>
            </dependency>

            <!-- Additional dependencies -->
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>
            <dependency>
                <groupId>org.awaitility</groupId>
                <artifactId>awaitility</artifactId>
                <version>${awaitility.version}</version>
            </dependency>
            <dependency>
                <groupId>org.bouncycastle</groupId>
                <artifactId>bcprov-jdk15on</artifactId>
                <version>${bouncycastle.version}</version>
            </dependency>
            <dependency>
                <groupId>org.codehaus.woodstox</groupId>
                <artifactId>stax2-api</artifactId>
                <version>${stax2-api.version}</version>
            </dependency>
            <dependency>
                <groupId>org.codehaus.woodstox</groupId>
                <artifactId>woodstox-core-asl</artifactId>
                <version>${woodstox-core.version}</version>
            </dependency>

            <!-- Test dependencies -->
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-core</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-spring</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>cglib</groupId>
                <artifactId>cglib-nodep</artifactId>
                <version>${cglib.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.objenesis</groupId>
                <artifactId>objenesis</artifactId>
                <version>${objenesis.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- Test dependencies -->
        <dependency>
            <groupId>org.spockframework</groupId>
            <artifactId>spock-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.awaitility</groupId>
            <artifactId>awaitility</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>cglib</groupId>
            <artifactId>cglib-nodep</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.objenesis</groupId>
            <artifactId>objenesis</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco-maven-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.gmavenplus</groupId>
                    <artifactId>gmavenplus-plugin</artifactId>
                    <version>${gmavenplus-plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <dependencies>
                    <dependency>
                        <groupId>org.ow2.asm</groupId>
                        <artifactId>asm</artifactId>
                        <version>${asm.version}</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <encoding>${sourceEncoding}</encoding>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
                <configuration>
                    <!-- FIXME see: https://github.com/spockframework/spock/pull/900 -->
                    <targetBytecode>1.8</targetBytecode>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>addTestSources</goal>
                            <goal>compileTests</goal>
                            <goal>removeTestStubs</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <append>true</append>
                    <destFile>${sonar.jacoco.reportPaths}</destFile>
                </configuration>
                <executions>
                    <execution>
                        <id>agent-for-unit-tests</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>jacoco-site</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
