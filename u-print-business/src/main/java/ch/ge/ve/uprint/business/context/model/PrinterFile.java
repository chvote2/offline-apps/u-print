/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.model;

import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import com.google.common.collect.ImmutableList;
import java.nio.file.Path;
import java.util.List;

/**
 * A value object that holds all of components of a printer file.
 */
public class PrinterFile {
  private final Path                            source;
  private final List<Path>                      operationReferenceFiles;
  private final Path                            ech0228FilePath;
  private final PrinterOperationConfigurationVo operationConfiguration;

  /**
   * Create a new {@link PrinterFile} instance.
   *
   * @param source                  the unmodified location of the source file.
   * @param operationReferenceFiles the locations of the operation reference files (eCH-0157 and eCH-0159).
   * @param ech0228FilePath         the location of the eCH-0228.
   * @param operationConfiguration  the operation configuration as it appears in the eCH-0228.
   */
  public PrinterFile(Path source,
                     List<Path> operationReferenceFiles,
                     Path ech0228FilePath,
                     PrinterOperationConfigurationVo operationConfiguration) {
    this.source = source;
    this.operationReferenceFiles = ImmutableList.copyOf(operationReferenceFiles);
    this.ech0228FilePath = ech0228FilePath;
    this.operationConfiguration = operationConfiguration;
  }

  public Path getSource() {
    return source;
  }

  /**
   * Return all the operation reference files (eCH-0157 and eCH-0159) contained in this printer file. The returned
   * collection is immutable.
   *
   * @return the list of operation reference files.
   */
  public List<Path> getOperationReferenceFiles() {
    return operationReferenceFiles;
  }

  public Path getEch0228FilePath() {
    return ech0228FilePath;
  }

  public PrinterOperationConfigurationVo getOperationConfiguration() {
    return operationConfiguration;
  }
}
