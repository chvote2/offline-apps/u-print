/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228;

import ch.ge.ve.interfaces.ech.eCH0044.v4.PersonIdentificationLightType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.PersonIdentificationType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.ForeignerPersonType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.ForeignerType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.SwissAbroadType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.SwissDomesticType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.SwissPersonType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingPersonType;
import java.util.stream.Collectors;

/**
 * A utility class that contains common functionality to retrieve and create eCH-0228 entities.
 */
public class ECH0228Utils {

  public static final String ECH_0228_NS = "http://www.ech.ch/xmlns/eCH-0228/1";
  public static final String ECH_0228_P  = "e228";

  public static final String DELIVERY             = "delivery";
  public static final String DELIVERY_HEADER      = "deliveryHeader";
  public static final String VOTING_CARD_DELIVERY = "votingCardDelivery";
  public static final String CONTEST_DATA         = "contestData";
  public static final String VOTING_CARD          = "votingCard";
  public static final String VOTER                = "voter";

  /**
   * Map the given eCH-0045 {@link ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType} to an eCH-0228 compliant {@link
   * VotingPersonType}.
   *
   * @param votingPersonType the eCH-0045 {@link ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType} to map.
   *
   * @return the mapped eCH-0228 {@link VotingPersonType}.
   */
  public static VotingPersonType mapECH0045VotingPersonType(
      ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType votingPersonType
  ) {
    VotingPersonType result = new VotingPersonType();

    result.setDeliveryAddress(votingPersonType.getDeliveryAddress());
    votingPersonType.getPerson().getSwiss();
    result.setPerson(mapECH0045Person(votingPersonType.getPerson()));

    result.getDomainOfInfluenceInfo().addAll(
        votingPersonType.getDomainOfInfluenceInfo()
                        .stream()
                        .map(ECH0228Utils::mapDomainOfInfluenceInfo)
                        .collect(Collectors.toList())
    );

    result.setIsEvoter(true);

    return result;
  }

  /**
   * Retrieve the person id of an eCH-0228 {@link VotingCardDeliveryType.VotingCard} entity.
   *
   * @param votingCard the eCH-0228 {@link VotingCardDeliveryType.VotingCard} entity.
   *
   * @return the person id.
   */
  public static String retrievePersonId(VotingCardDeliveryType.VotingCard votingCard) {
    VotingPersonType.Person person = votingCard.getVotingPerson().getPerson();
    PersonIdentificationLightType personIdentificationType;

    if (person.getSwiss() != null) {
      personIdentificationType = person.getSwiss().getSwissDomesticPerson().getPersonIdentification();
    } else if (person.getForeigner() != null) {
      personIdentificationType = person.getForeigner().getForeignerPerson().getPersonIdentification();
    } else if (person.getSwissAbroad() != null) {
      personIdentificationType = person.getSwissAbroad().getSwissAbroadPerson().getPersonIdentification();
    } else {
      throw new IllegalArgumentException("The person type must contain an identification");
    }

    return personIdentificationType.getLocalPersonId().getPersonId();
  }

  private static VotingPersonType.DomainOfInfluenceInfo mapDomainOfInfluenceInfo(
      ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType.DomainOfInfluenceInfo domainOfInfluenceInfo
  ) {
    VotingPersonType.DomainOfInfluenceInfo result = new VotingPersonType.DomainOfInfluenceInfo();

    result.setCountingCircle(domainOfInfluenceInfo.getCountingCircle());
    result.setDomainOfInfluence(domainOfInfluenceInfo.getDomainOfInfluence());

    return result;
  }

  private static VotingPersonType.Person mapECH0045Person(
      ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType.Person person
  ) {
    VotingPersonType.Person result = new VotingPersonType.Person();

    if (person.getSwiss() != null) {
      SwissDomesticType swiss = new SwissDomesticType();
      swiss.setMunicipality(person.getSwiss().getMunicipality());
      swiss.setSwissDomesticPerson(mapECH0045SwissPersonType(person.getSwiss().getSwissDomesticPerson()));

      result.setSwiss(swiss);
    }

    if (person.getForeigner() != null) {
      ForeignerType foreigner = new ForeignerType();
      foreigner.setMunicipality(person.getForeigner().getMunicipality());
      foreigner.setForeignerPerson(mapECH0045ForeignerPersonType(person.getForeigner().getForeignerPerson()));
      result.setForeigner(foreigner);
    }

    if (person.getSwissAbroad() != null) {
      SwissAbroadType swissAbroad = new SwissAbroadType();
      swissAbroad.setCanton(person.getSwissAbroad().getCanton());
      swissAbroad.setDateOfRegistration(person.getSwissAbroad().getDateOfRegistration());
      swissAbroad.setMunicipality(person.getSwissAbroad().getMunicipality());
      swissAbroad.setResidenceCountry(person.getSwissAbroad().getResidenceCountry());
      swissAbroad.setSwissAbroadPerson(mapECH0045SwissPersonType(person.getSwissAbroad().getSwissAbroadPerson()));

      result.setSwissAbroad(swissAbroad);
    }

    return result;
  }

  private static ForeignerPersonType mapECH0045ForeignerPersonType(
      ch.ge.ve.interfaces.ech.eCH0045.v4.ForeignerPersonType foreignerPersonType
  ) {
    ForeignerPersonType result = new ForeignerPersonType();

    result.setDeclaredForeignName(foreignerPersonType.getDeclaredForeignName());
    result.setNameOnForeignPassport(foreignerPersonType.getNameOnForeignPassport());
    result.setResidencePermit(foreignerPersonType.getResidencePermit());
    result.setAllianceName(foreignerPersonType.getAllianceName());
    result.setCallName(foreignerPersonType.getCallName());
    result.setExtension(foreignerPersonType.getExtension());
    result.setLanguageOfCorrespondance(foreignerPersonType.getLanguageOfCorrespondance());
    result.setPersonIdentification(mapECH0045PersonIdentificationType(foreignerPersonType.getPersonIdentification()));
    result.setReligionData(foreignerPersonType.getReligionData());

    return result;
  }

  private static SwissPersonType mapECH0045SwissPersonType(
      ch.ge.ve.interfaces.ech.eCH0045.v4.SwissPersonType swissPersonType
  ) {
    SwissPersonType result = new SwissPersonType();

    result.setAllianceName(swissPersonType.getAllianceName());
    result.setCallName(swissPersonType.getCallName());
    result.setExtension(swissPersonType.getExtension());
    result.setLanguageOfCorrespondance(swissPersonType.getLanguageOfCorrespondance());
    result.setPersonIdentification(mapECH0045PersonIdentificationType(swissPersonType.getPersonIdentification()));
    result.setReligionData(swissPersonType.getReligionData());

    swissPersonType.getPlaceOfOrigin().forEach(
        placeOfOriginType -> result.getPlaceOfOrigin().add(placeOfOriginType)
    );
    return result;
  }

  private static PersonIdentificationLightType mapECH0045PersonIdentificationType(
      PersonIdentificationType personIdentificationType
  ) {
    PersonIdentificationLightType result = new PersonIdentificationLightType();

    result.setVn(personIdentificationType.getVn());
    result.setDateOfBirth(personIdentificationType.getDateOfBirth());
    result.setFirstName(personIdentificationType.getFirstName());
    result.setLocalPersonId(personIdentificationType.getLocalPersonId());
    result.setOfficialName(personIdentificationType.getOfficialName());
    result.setOriginalName(personIdentificationType.getOriginalName());
    result.setSex(personIdentificationType.getSex());

    return result;
  }

  /**
   * Hide utility class constructor.
   */
  private ECH0228Utils() {
    throw new AssertionError("Not instantiable");
  }

}
