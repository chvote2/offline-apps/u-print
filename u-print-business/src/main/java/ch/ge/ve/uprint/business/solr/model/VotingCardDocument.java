/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr.model;

import org.apache.solr.client.solrj.beans.Field;

/**
 * The data for a voting card as a document in the Solr index. Used to store the voting card into the Solr index as well
 * as to query voting cards from the Solr index.
 */
public class VotingCardDocument {
  @Field
  private String voterId;

  @Field
  private String votingCardXmlElement;

  /**
   * Create a new voting card document with the given parameters.
   *
   * @param voterId              The unique identifier of a voter, that is, the value of a <personId> element of an
   *                             eCH-0228 XML file.
   * @param votingCardXmlElement The complete raw XML element of a voting card, that is, a <votingCard> element in an
   *                             eCH-0228 XML file. Includes all necessary namespace information.
   */
  public VotingCardDocument(String voterId, String votingCardXmlElement) {
    this.voterId = voterId;
    this.votingCardXmlElement = votingCardXmlElement;
  }

  /**
   * Create an empty  voting card document. Required by solr in order to create the object with Reflection.
   */
  public VotingCardDocument() {

  }

  public String getVoterId() {
    return voterId;
  }

  public void setVoterId(String voterId) {
    this.voterId = voterId;
  }

  public String getVotingCardXmlElement() {
    return votingCardXmlElement;
  }

  public void setVotingCardXmlElement(String votingCardXmlElement) {
    this.votingCardXmlElement = votingCardXmlElement;
  }
}
