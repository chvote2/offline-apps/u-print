/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business

import ch.ge.ve.filenamer.PrinterArchiveFileName
import groovy.transform.CompileStatic
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDateTime
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream
import org.junit.rules.TemporaryFolder

/**
 * A helper class for tests that manipulates archives (Zip files) with optional filtering and renaming.
 */
@CompileStatic
class ArchiveHelper {

  private Closure<Path> archiveFolderSupplier

  /**
   * Instantiates based on a TemporaryFolder rule - uses a managed folder as the zip destination directory
   *
   * @param temporaryFolder the TemporaryFolder rule
   */
  ArchiveHelper(TemporaryFolder temporaryFolder) {
    this.archiveFolderSupplier = { temporaryFolder.newFolder("archives").toPath() }.memoize()
  }

  /**
   * Copies an archive and changes its name to a valid formatted "archive printer filename"
   *
   * @param resource path to the archive
   * @return a path to the copied archive
   */
  Path copyArchiveWithFormattedName(Path resource) {
    def inputStream = Files.newInputStream(resource)
    return inputStream.withCloseable { InputStream it ->
      def path = archiveFolderSupplier().resolve(createArchiveName())
      Files.copy(it, path)
      return path
    }
  }

  /**
   * Repackages an archive, excluding some entries, and changes its name to a valid formatted "archive printer filename"
   *
   * @param resource path to the archive
   * @param excludedEntries a predicate closure to define excluded entries (by name)
   * @return a path to the copied archive
   */
  Path repackageWithFormattedName(Path resource,
                                  @ClosureParams(value = SimpleType, options = "java.lang.String") Closure<Boolean> excludedEntries) {
    def output = archiveFolderSupplier().resolve(createArchiveName())
    def inputStream = Files.newInputStream(resource)

    filterEntries(new ZipInputStream(inputStream), new ZipOutputStream(Files.newOutputStream(output)), excludedEntries)
    return output
  }

  /**
   * Repackages an archive, excluding some entries, and changes its name to "repackaged-ORIGINALNAME"
   *
   * @param resource path to the archive
   * @param excludedEntries a predicate closure to define excluded entries (by name)
   *
   * @return a path to the copied archive
   */
  Path repackage(Path resource, @ClosureParams(value = SimpleType, options = "java.lang.String") Closure<Boolean> excludedEntries) {
    def originalName = resource.getFileName().toString()
    def output = archiveFolderSupplier().resolve("repackaged-$originalName")

    def inputStream = Files.newInputStream(resource)
    filterEntries(new ZipInputStream(inputStream), new ZipOutputStream(Files.newOutputStream(output)), excludedEntries)
    return output
  }

  private static void filterEntries(ZipInputStream instream, ZipOutputStream outstream, Closure<Boolean> excludedEntries) {
    try {
      ZipEntry entry
      while ((entry = instream.nextEntry) != null) {
        if (!excludedEntries(entry.name)) {
          outstream.putNextEntry(new ZipEntry(entry.name))
          outstream.write(instream.readAllBytes())
          outstream.closeEntry()
        }
      }
    } finally {
      instream.close()
      outstream.close()
    }
  }

  private static String createArchiveName() {
    return new PrinterArchiveFileName("test printer", "automatic tests", LocalDateTime.now()).toString()
  }

}
