/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.uprint.business.context.factory.ControlComponentsPublicKeysFileFactory
import ch.ge.ve.uprint.business.context.factory.PrinterArchiveFactory
import ch.ge.ve.uprint.business.context.factory.PrivateKeyFactory
import ch.ge.ve.uprint.business.context.model.Context
import groovy.transform.CompileStatic
import java.nio.file.Path

@CompileStatic
class ContextHelper {

  private ControlComponentsPublicKeysFileFactory controlComponentsPublicKeysFileFactory
  private PrinterArchiveFactory printerArchiveFactory
  private PrivateKeyFactory privateKeyFactory
  private ArchiveHelper archiveHelper

  ContextHelper(ControlComponentsPublicKeysFileFactory controlComponentsPublicKeysFileFactory,
                PrinterArchiveFactory printerArchiveFactory,
                PrivateKeyFactory privateKeyFactory,
                ArchiveHelper archiveHelper) {
    this.controlComponentsPublicKeysFileFactory = controlComponentsPublicKeysFileFactory
    this.printerArchiveFactory = printerArchiveFactory
    this.privateKeyFactory = privateKeyFactory
    this.archiveHelper = archiveHelper
  }

  def createContext(Path printerArchive, Path outputDir, ProgressTracker progressTracker) {
    def context = new Context()

    context.setControlComponentsPublicKeysFile(
            controlComponentsPublicKeysFileFactory.readControlComponentsPublicKeysFile(TestResources.CONTROL_COMPONENT_KEYS)
    )

    context.setPrinterArchive(
            printerArchiveFactory.readPrinterArchive(
                    archiveHelper.copyArchiveWithFormattedName(printerArchive),
                    progressTracker)
    )

    context.setPrinterDecryptionKey(
            privateKeyFactory.readPrivateKey(
                    TestResources.PRIVATE_KEY,
                    "simulation password!".toCharArray())
    )

    context.setPrinterFileDestination(outputDir)

    return context
  }
}
