/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.uprint.business.CryptoTestConfig
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.context.api.ContextService
import ch.ge.ve.uprint.business.context.factory.PrinterArchiveFactory
import ch.ge.ve.uprint.business.context.factory.PrivateKeyFactory
import ch.ge.ve.uprint.business.context.model.PrinterArchive
import java.nio.file.Paths
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = [CryptoTestConfig])
class ContextServiceImplTest extends Specification {
  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  @Autowired
  PublicParameters publicParameters

  PrinterArchiveFactory printerArchiveFactory
  ContextService contextService
  PrivateKeyFactory privateKeyFactory

  def setup() {
    printerArchiveFactory = Mock(PrinterArchiveFactory)
    privateKeyFactory = new PrivateKeyFactory(publicParameters)
    contextService = new ContextServiceImpl(printerArchiveFactory)
  }

  def "should setup the context"() {
    given:
    def privateKey = getPrivateKey()
    def printerArchiveLocation = Paths.get("printerArchiveLocation")
    def printerFileDestination = temporaryFolder.newFolder().toPath()
    def tracker = Mock(ProgressTracker)
    printerArchiveFactory.readPrinterArchive(printerArchiveLocation, tracker) >> Mock(PrinterArchive)

    when:
    contextService.setupPrinterArchiveContext(privateKey, printerArchiveLocation, printerFileDestination, tracker)
    def context = contextService.get()

    then:
    context.getControlComponentsPublicKeysFile() == null
    context.getPrinterFile() == null
    context.getPrinterArchive() != null
    context.getPrinterDecryptionKey() != null
    context.getPrinterFileDestination().startsWith(printerFileDestination)
    context.getPrinterFileDestination().toFile().exists()
  }

  def getPrivateKey() {
    def privateKeySource = TestResources.PRIVATE_KEY
    return privateKeyFactory.readPrivateKey(privateKeySource, "simulation password!".toCharArray())
  }
}
