/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory

import ch.ge.ve.filenamer.archive.InvalidArchiveException
import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.uprint.business.ArchiveHelper
import ch.ge.ve.uprint.business.TestResources
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import spock.util.environment.RestoreSystemProperties

class PrinterArchiveFactoryTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  ArchiveHelper helper = new ArchiveHelper(temporaryFolder)

  PrinterArchiveFactory printerArchiveFactory

  def setup() {
    printerArchiveFactory = new PrinterArchiveFactory()
  }

  def "should create printer archive from a valid zip"() {
    given:
    def source = helper.copyArchiveWithFormattedName(TestResources.PRINTER_ARCHIVE_VALID_STANDARD_BALLOT)

    when:
    def archive = printerArchiveFactory.readPrinterArchive(source, Mock(ProgressTracker))

    then:
    archive.getEch0045() != null
    archive.getPublicParametersFile() != null
    archive.getElectionSetFile() != null
    archive.getOperationConfigurationFile() != null
    archive.getOperationReferenceFiles() != null
    archive.getPrivateCredentials().size() == 2
  }

  @RestoreSystemProperties
  def "should extract the archive in a given folder if specified"() {
    given:
    def outputFolder = temporaryFolder.newFolder("output")
    System.setProperty("extract.to.path", outputFolder.absolutePath)
    def source = helper.copyArchiveWithFormattedName(TestResources.PRINTER_ARCHIVE_VALID_STANDARD_BALLOT)

    when:
    def archive = printerArchiveFactory.readPrinterArchive(source, Mock(ProgressTracker))

    then:
    outputFolder.list().length == 1
    outputFolder.list()[0] =~ "^printer-archive_automatic-tests_test-printer_(.*)\$"

    archive.ech0045.parent == outputFolder.listFiles()[0].toPath()
  }

  def "should fail to create printer archive if there is no ech0045"() {
    given:
    def missingEch45 = helper.repackageWithFormattedName(TestResources.PRINTER_ARCHIVE_VALID_STANDARD_BALLOT) {
      String entry -> entry.startsWith("eCH-0045")
    }

    when:
    printerArchiveFactory.readPrinterArchive(missingEch45, Mock(ProgressTracker))

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'Some mandatory files are missing : [eCH-0045.xml]'
  }

  def "should fail to create printer archive if there is no private credentials file"() {
    given:
    def missingEpf = helper.repackageWithFormattedName(TestResources.PRINTER_ARCHIVE_VALID_STANDARD_BALLOT) {
      String entry -> entry.endsWith("epf")
    }

    when:
    printerArchiveFactory.readPrinterArchive(missingEpf, Mock(ProgressTracker))

    then:
    def ex = thrown InvalidArchiveException
    ex.message == 'No files were found for some expected groups : [private credentials]'
  }
}
