/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.solr.EmbeddedSolrService
import ch.ge.ve.uprint.business.solr.api.SolrService
import ch.ge.ve.uprint.business.solr.factory.SolrServerFactory
import java.nio.file.Path
import spock.lang.Specification

class VotingCardNavigatorTest extends Specification {
  SolrService solrService

  def setup() {
    solrService = new EmbeddedSolrService(new SolrServerFactory().getObject())
  }

  def cleanup() {
    solrService.deleteAll()
    solrService.close()
  }

  def "should navigate an imported eCH-0228"() {
    given:
    importPrinterFile(TestResources.ECH0228)

    when:
    def navigator = new VotingCardNavigator(solrService.getClient())

    then:
    navigator.size() == 11
    navigator.currentIndex() == -1
    !navigator.hasPrevious()
    navigator.hasNext()
    ECH0228Utils.retrievePersonId(navigator.next()) == "0ohQ5VPXbtiFbq2XDSOGJ4uJ_MT0cr-yaPPC"
    navigator.currentIndex() == 0
    !navigator.hasPrevious()
    navigator.hasNext()
    ECH0228Utils.retrievePersonId(navigator.last()) == "hTrAMFTAZiJykwQeBmjOmPIuPmSDQrQK8XeX"
    navigator.currentIndex() == 10
    navigator.hasPrevious()
    !navigator.hasNext()
    ECH0228Utils.retrievePersonId(navigator.previous()) == "WzHRUjNBFv_pe-ak9zwknMn7uFde2GFkXMc0"
    navigator.currentIndex() == 9
    navigator.hasPrevious()
    navigator.hasNext()
    ECH0228Utils.retrievePersonId(navigator.first()) == "0ohQ5VPXbtiFbq2XDSOGJ4uJ_MT0cr-yaPPC"
    navigator.currentIndex() == 0
    !navigator.hasPrevious()
    navigator.hasNext()
  }

  def "should fail to call previous if there are no previous elements"() {
    given:
    importPrinterFile(TestResources.ECH0228)

    when:
    def navigator = new VotingCardNavigator(solrService.getClient())
    navigator.first()
    navigator.previous()

    then:
    thrown(NoSuchElementException)
  }

  def "should fail to call next if there are no more elements"() {
    given:
    importPrinterFile(TestResources.ECH0228)

    when:
    def navigator = new VotingCardNavigator(solrService.getClient())
    navigator.last()
    navigator.next()

    then:
    thrown(NoSuchElementException)
  }

  def importPrinterFile(Path source) {
    def solrServer = solrService.getClient()
    new ECH0228Parser(solrServer, Mock(ProgressTracker), source).doImport()
  }
}
