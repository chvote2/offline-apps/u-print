/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.key

import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms
import ch.ge.ve.protocol.model.IdentificationPublicKey
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.uprint.business.CryptoTestConfig
import ch.ge.ve.uprint.business.context.exception.InvalidPrivateKeyException
import ch.ge.ve.uprint.business.context.factory.PrivateKeyFactory
import java.nio.file.Files
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = [CryptoTestConfig])
class PrinterKeyGeneratorTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder

  @Autowired
  JsonPathMapper jsonPathMapper

  @Autowired
  PublicParameters publicParameters

  @Autowired
  KeyEstablishmentAlgorithms keyEstablishmentAlgorithms

  PrinterKeyGenerator generator

  def setup() {
    generator = new PrinterKeyGenerator(jsonPathMapper, publicParameters, keyEstablishmentAlgorithms)
  }

  def "should generate a readable pair of keys"() {
    given:
    def passphrase = "1234"
    def output = temporaryFolder.newFolder().toPath()
    def privateKeyFactory = new PrivateKeyFactory(publicParameters)

    when:
    generator.generateAndStoreKeyPair(output, passphrase)

    then:
    def privateKeyFile =
            Files.list(output).
                    filter({ f -> f.fileName.toString().startsWith("private-key") })
                    .findFirst().get()
    def publicKeyFile =
            Files.list(output)
                    .filter({ f -> f.fileName.toString().startsWith("public-key") })
                    .findFirst().get()

    Files.exists(privateKeyFile)
    Files.exists(publicKeyFile)

    jsonPathMapper.map(publicKeyFile, IdentificationPublicKey.class) != null
    privateKeyFactory.readPrivateKey(privateKeyFile, "1234".toCharArray()) != null
  }

  def "should fail to read a private key with the wrong password"() {
    given:
    def passphrase = "1234"
    def output = temporaryFolder.newFolder().toPath()
    def privateKeyFactory = new PrivateKeyFactory(publicParameters)
    generator.generateAndStoreKeyPair(output, passphrase)

    when:
    privateKeyFactory.readPrivateKey(output.resolve("private-key.pfx"), "12345".toCharArray())

    then:
    thrown(InvalidPrivateKeyException)
  }

}
