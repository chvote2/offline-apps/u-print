/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr

import ch.ge.ve.uprint.business.solr.api.UprintSolrCore
import ch.ge.ve.uprint.business.solr.model.VoterDocument
import org.apache.solr.client.solrj.SolrClient
import spock.lang.Specification

class SolrBatchInserterTest extends Specification {

  def "adding 1 voter should not cause a commit when batch size is greater than 1"() {
    given:
    def solrClient = Mock(SolrClient)
    def proxy = new SolrBatchInserter(solrClient, UprintSolrCore.ELECTORAL_REGISTER, 2)

    when:
    proxy.add(new VoterDocument())

    then:
    0 * solrClient.commit(UprintSolrCore.ELECTORAL_REGISTER.getCoreName())
  }

  def "adding 5 voter should commit twice when batch size is 2"() {
    given:
    def solrClient = Mock(SolrClient)
    def proxy = new SolrBatchInserter(solrClient, UprintSolrCore.ELECTORAL_REGISTER, 2)

    when:
    proxy.add(new VoterDocument())
    proxy.add(new VoterDocument())
    proxy.add(new VoterDocument())
    proxy.add(new VoterDocument())
    proxy.add(new VoterDocument())

    then:
    2 * solrClient.commit(UprintSolrCore.ELECTORAL_REGISTER.getCoreName())
  }

}
