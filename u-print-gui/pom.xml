<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ u-print
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>ch.ge.ve.uprint</groupId>
        <artifactId>u-print-parent</artifactId>
        <version>0.0.1</version>
    </parent>

    <artifactId>u-print-gui</artifactId>

    <name>U-Print GUI</name>
    <description>JFX client for the U-Print application</description>
    <organization>
        <name>OCSIN-SIDP</name>
    </organization>

    <properties>
        <sonar.coverage.exclusions>
            src/main/java/ch/ge/ve/uprint/gui/controller/**/*,
            src/main/java/ch/ge/ve/uprint/gui/validator/**/*,
            src/main/java/ch/ge/ve/uprint/gui/Uprint.java
        </sonar.coverage.exclusions>
    </properties>

    <dependencies>
        <!-- CHVote dependencies -->
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>u-print-business</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.ui</groupId>
            <artifactId>chvote-fx-common</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>protocol-algorithms</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>protocol-support</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>chvote-protocol-model</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>pact-back-contract</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve</groupId>
            <artifactId>jackson-serializer</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>file-namer</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>chvote-interfaces-jaxb</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>chvote-model-converter-api</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.ge.ve.interfaces</groupId>
            <artifactId>chvote-model-converter-impl</artifactId>
        </dependency>

        <!-- JavaFX dependencies -->
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-controls</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-base</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-fxml</artifactId>
        </dependency>
        <dependency>
            <groupId>com.jfoenix</groupId>
            <artifactId>jfoenix</artifactId>
        </dependency>
        <dependency>
            <groupId>de.jensd</groupId>
            <artifactId>fontawesomefx-materialdesignfont</artifactId>
        </dependency>
        <dependency>
            <groupId>de.jensd</groupId>
            <artifactId>fontawesomefx-materialicons</artifactId>
        </dependency>

        <!-- Additional dependencies -->
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcprov-jdk15on</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jaxb</groupId>
            <artifactId>jaxb-runtime</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.woodstox</groupId>
            <artifactId>woodstox-core-asl</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <scope>runtime</scope>
        </dependency>
    </dependencies>

    <build>
        <finalName>u-print</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <finalName>${project.build.finalName}-${project.version}</finalName>
                    <appendAssemblyId>false</appendAssemblyId>
                    <descriptors>
                        <descriptor>src/assembly/assembly.xml</descriptor>
                    </descriptors>
                </configuration>
            </plugin>
            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <dotGitDirectory>${project.basedir}/.git</dotGitDirectory>
                    <commitIdGenerationMode>flat</commitIdGenerationMode>
                    <abbrevLength>8</abbrevLength>
                    <gitDescribe>
                        <skip>true</skip>
                    </gitDescribe>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>linux</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <dependencies>
                <dependency>
                    <groupId>org.openjfx</groupId>
                    <artifactId>javafx-graphics</artifactId>
                    <classifier>linux</classifier>
                </dependency>
            </dependencies>
        </profile>

        <profile>
            <id>windows</id>
            <dependencies>
                <dependency>
                    <groupId>org.openjfx</groupId>
                    <artifactId>javafx-graphics</artifactId>
                    <classifier>win</classifier>
                </dependency>
            </dependencies>
        </profile>

        <profile>
            <id>mac</id>
            <dependencies>
                <dependency>
                    <groupId>org.openjfx</groupId>
                    <artifactId>javafx-graphics</artifactId>
                    <classifier>mac</classifier>
                </dependency>
            </dependencies>
        </profile>
    </profiles>

</project>
